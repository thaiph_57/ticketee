require 'spec_helper'
feature "Viewing tickets" do
let!(:project) { FactoryGirl.create(:project) }
  let!(:user) { FactoryGirl.create(:user) }
  let!(:ticket) do
    ticket = FactoryGirl.create(:ticket, project: project)
    ticket.update(user: user)
    ticket
  end
  before do
    sign_in_as!(user)
    ticket = FactoryGirl.create(:ticket,  project: textmate_2,
                                title: "Make it shiny!",
                                description: "Gradients! Starbursts! Oh my!")
    ticket.update(user: user)
  end
  scenario "Viewing tickets for a given project" do
    click_link "TextMate 2"
    expect(page).to have_content("Make it shiny!")
    expect(page).to_not have_content("Standards compliance")
    click_link "Make it shiny!"
    within("#ticket h2") do
      expect(page).to have_content("Make it shiny!")
    end
    expect(page).to have_content("Gradients! Starbursts! Oh my!")
  end
end